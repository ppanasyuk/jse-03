# TASK MANAGER

## DEVELOPER INFO

* **Name**: Petr Panasyuk

* **E-mail**: ppanasyuk@t1-consulting.ru

## SOFTWARE

* **OS**: Windows 10

* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: i5

* **RAM**: 12GB

* **SSD**: 512GB

## PROGRAM RUN

```shell
java -jar ./task-manager.jar
```